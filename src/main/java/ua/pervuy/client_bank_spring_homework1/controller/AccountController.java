package ua.pervuy.client_bank_spring_homework1.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ua.pervuy.client_bank_spring_homework1.dto.account.AccountDtoIn;
import ua.pervuy.client_bank_spring_homework1.dto.TransferMoneyDtoIn;
import ua.pervuy.client_bank_spring_homework1.service.AccountService;

@RestController
@RequiredArgsConstructor
public class AccountController {

      private final AccountService accountService;

    //    Поповнити рахунок (приймає номер рахунку та суму)
    //    {
    //        "number": "eaa3c1b0-625d-409f-8a62-391447a78d9c",
    //            "summa": 1000
    //    }
    @PostMapping("account/top-up")
    public boolean topUpAccount(@RequestBody AccountDtoIn accountDtoIn){

        return accountService.topUpAccount(accountDtoIn);
    }

    //    зняти гроші з рахунку (приймає номер рахунку та суму, виконується тільки якщо на рахунку достатньо грошей)
    //    {
    //        "number": "eaa3c1b0-625d-409f-8a62-391447a78d9c",
    //        "summa": 900
    //    }
    @PostMapping("account/withdraw")
    public boolean withdraw(@RequestBody AccountDtoIn accountDtoIn){

        return accountService.withdraw(accountDtoIn);
    }

    //    переказати гроші на інший рахунок (приймає два номери рахунку та суму,
    //    виконується тільки якщо на рахунку достатньо грошей)
    //    {
    //        "number_from": "eaa3c1b0-625d-409f-8a62-391447a78d9c",
    //            "number_to": "6f073347-d8b9-4562-a477-b688d499be3e",
    //            "summa": 100
    //    }
    @PostMapping("account/transferMoney")
    public boolean transferMoney(@RequestBody TransferMoneyDtoIn transferMoneyDtoIn){

        return accountService.transferMoney(transferMoneyDtoIn);
    }
}
