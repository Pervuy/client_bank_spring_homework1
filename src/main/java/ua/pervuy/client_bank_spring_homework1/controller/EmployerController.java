package ua.pervuy.client_bank_spring_homework1.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import ua.pervuy.client_bank_spring_homework1.dto.customer.CustomerDtoIn;
import ua.pervuy.client_bank_spring_homework1.dto.employer.EmployerDtoIn;
import ua.pervuy.client_bank_spring_homework1.dto.employer.EmployerDtoOut;
import ua.pervuy.client_bank_spring_homework1.dto.employer.EmployerSimpleDtoOut;
import ua.pervuy.client_bank_spring_homework1.service.EmployerService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class EmployerController {

    private final EmployerService employerService;

    @GetMapping("employer/{id}")
    public EmployerDtoOut getEmployerByID(@PathVariable("id") Long idEmployer){
        return employerService.getEmployerByID(idEmployer);
    }
    @GetMapping("employer")
    public List<EmployerDtoOut> getAllEmployers(@RequestParam(value = "page",defaultValue = "0") Integer page,
                                                @RequestParam(value = "size",defaultValue = "10") Integer size){
        return employerService.getAllEmployers(page, size);
    }

    @PostMapping("employer")
    public EmployerSimpleDtoOut createEmployer(@Valid  @RequestBody EmployerDtoIn employerDtoIn){

        return employerService.save(employerDtoIn);
    }
    @PutMapping("employer")
    public EmployerDtoOut editeEmployer(@Valid @RequestBody EmployerDtoIn employerDtoIn){

        return employerService.editeEmployer(employerDtoIn);
    }

    @DeleteMapping("employer/{id}")
    public boolean deleteEmployer(@PathVariable("id") Long idEmployer){
        return employerService.deleteEmployer(idEmployer);
    }

    //    {
    //        "id": 1
    //    }
    @PostMapping("employer/{id}/customer")
    public EmployerDtoOut addCustomerFromEmployer(@PathVariable("id") Long idEmployer, @RequestBody CustomerDtoIn customerDtoIn){

        return employerService.addCustomerFromEmployer(idEmployer, customerDtoIn);
    }

    //    {
    //        "id": 1
    //    }
    @DeleteMapping("employer/{id}/customer")
    public EmployerDtoOut deleteCustomerFromEmployer(@PathVariable("id") Long idEmployer, @RequestBody CustomerDtoIn customerDtoIn){

        return employerService.deleteCustomerFromEmployer(idEmployer, customerDtoIn);
    }
}
