package ua.pervuy.client_bank_spring_homework1.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ua.pervuy.client_bank_spring_homework1.dto.account.AccountDtoIn;
import ua.pervuy.client_bank_spring_homework1.dto.account.AccountDtoOut;
import ua.pervuy.client_bank_spring_homework1.dto.customer.CustomerDtoIn;
import ua.pervuy.client_bank_spring_homework1.dto.customer.CustomerDtoOut;
import ua.pervuy.client_bank_spring_homework1.dto.customer.CustomerSimpleDtoOut;
import ua.pervuy.client_bank_spring_homework1.service.CustomerService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService customerService;

    // Отримати інформацію про окремого користувача, включаючи його рахунки
    @GetMapping("user/{id}")
    public CustomerDtoOut getUserByID(@PathVariable("id") Long idCustomer) {
        return customerService.getUserByID(idCustomer);
    }

    // Отримати інформацію про всіх користувачів
    @GetMapping("users")
    public List<CustomerDtoOut> getAllCustomers(@RequestParam(value = "page",defaultValue = "0") Integer page,
                                                @RequestParam(value = "size",defaultValue = "10") Integer size){

        return customerService.getAllCustomers(page, size);
    }

    //    Створити користувача
    //    {
    //        "name":"Alex2",
    //        "email":"qqq2@gmail",
    //        "age":32
    //    }
    @PostMapping("user")
    public CustomerSimpleDtoOut createCustomer(@Valid @RequestBody CustomerDtoIn customerDtoIn){

        return customerService.save(customerDtoIn);
    }

    //    Змінити дані користувача
    //    {
    //        "name":"Alex",
    //        "email":"qqq2@gmail",
    //        "age":33,
    //        "id":1
    //    }
    @PutMapping("user")
    public CustomerDtoOut editeCustomer(@Valid @RequestBody CustomerDtoIn customerDtoIn){

        return customerService.editeCustomer(customerDtoIn);
    }

    //    Видалити користувача
    @DeleteMapping("user/{id}")
    public boolean deleteCustomer(@PathVariable("id") Long id){

        return customerService.deleteCustomer(id);
    }


    // Створити рахунок для конкретного користувача
    @PostMapping("user/{id}/account")
    public AccountDtoOut addAccount(@Valid @PathVariable("id") Long idCustomer, @RequestBody AccountDtoIn accountDtoIn){
        return customerService.addAccount(idCustomer, accountDtoIn);
    }

    //    Видалити рахунок у користувача
    //    {
    //        "number": "3cf52874-cd8d-4200-b09a-6798eeaa22bb"
    //    }
    @DeleteMapping("user/{id}/account")
    public boolean deleteAccount(@PathVariable("id") Long idCustomer, @RequestBody AccountDtoIn accountDtoIn) {
        return customerService.deleteAccount(idCustomer, accountDtoIn);
    }
}
