package ua.pervuy.client_bank_spring_homework1.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.repository.config.BootstrapMode;

import javax.sql.DataSource;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "employers")
public class Employer extends AbstractEntity{
    private String name;
    private String address;

    @ManyToMany
    @JoinTable(
            name = "employer_customer",
            joinColumns = @JoinColumn(name = "customer_id"),
            inverseJoinColumns = @JoinColumn(name = "employer_id"))
    Set<Customer> customers;

    public Employer(){

    }


}
