package ua.pervuy.client_bank_spring_homework1.entity;

public enum Currency {
    USD,
    EUR,
    UAH,
    CHF,
    GBP
}
