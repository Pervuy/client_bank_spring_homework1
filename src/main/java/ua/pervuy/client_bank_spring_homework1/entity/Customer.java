package ua.pervuy.client_bank_spring_homework1.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.util.*;

@Getter
@Setter
@Entity
@Table(name = "customers")
public class Customer extends AbstractEntity {
    private String name;
    private String email;
    private Integer age;
    private String password;

    private String phone;

   // @JsonManagedReference
    @OneToMany(mappedBy = "customer")
    private List<Account> accounts;

    @ManyToMany(mappedBy = "customers")
    private Set<Employer> employers;

    public Customer(){
        
    }

    public Customer(String name, String email, Integer age) {
        this.name = name;
        this.email = email;
        this.age = age;
        this.accounts = new ArrayList<>();
        this.employers = new HashSet<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return this.getId().equals(customer.getId()) && email.equals(customer.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getId(), email);
    }

    public boolean addAccount(Account account){
       return this.accounts.add(account);
    }
    public boolean deleteAccount(Account account){
        return this.accounts.remove(account);
    }
}
