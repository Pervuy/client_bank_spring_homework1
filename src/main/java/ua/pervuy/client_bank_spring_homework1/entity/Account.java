package ua.pervuy.client_bank_spring_homework1.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "accounts")
public class Account extends AbstractEntity {
    private String number;
    private Currency currency;
    private Double balance;
    //@JsonBackReference
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    public Account(){}

    public Account(Customer customer, Currency currency) {
        this.currency = currency;
        this.customer = customer;
        this.number = UUID.randomUUID().toString();
        this.balance = 0.0;
    }

    public boolean isEnoughMoney(Integer summa){
        return this.balance >= ((double)summa/100);
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return this.getId().equals(account.getId()) && number.equals(account.number) && currency == account.currency && customer.equals(account.customer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getId(), number, currency, customer);
    }

    public Long getCustomerId(){
        return this.customer.getId();
    }

    public String getCustomerName(){
        return this.customer.getName();
    }
}
