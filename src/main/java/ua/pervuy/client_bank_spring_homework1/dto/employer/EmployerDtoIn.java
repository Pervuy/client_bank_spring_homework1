package ua.pervuy.client_bank_spring_homework1.dto.employer;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class EmployerDtoIn {

    private Long id;
    @Size(min = 3, message = "Name should be more that 3 character")
    private String name;
    @Size(min = 3, message = "Address should be more that 3 character")
    private String address;
}
