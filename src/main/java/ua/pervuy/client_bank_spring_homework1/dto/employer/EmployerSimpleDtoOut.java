package ua.pervuy.client_bank_spring_homework1.dto.employer;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class EmployerSimpleDtoOut {

    private Long id;
    private String name;
    private String address;

}
