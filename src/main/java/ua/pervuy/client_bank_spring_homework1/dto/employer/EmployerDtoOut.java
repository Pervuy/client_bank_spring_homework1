package ua.pervuy.client_bank_spring_homework1.dto.employer;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import ua.pervuy.client_bank_spring_homework1.dto.customer.CustomerSimpleDtoOut;

import java.util.List;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class EmployerDtoOut {

    private Long id;
    private String name;
    private String address;
    List<CustomerSimpleDtoOut> customers;
}
