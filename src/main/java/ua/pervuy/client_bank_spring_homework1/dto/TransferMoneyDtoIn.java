package ua.pervuy.client_bank_spring_homework1.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class TransferMoneyDtoIn {
    private String numberFrom;
    private String numberTo;
    private Integer summa;
}
