package ua.pervuy.client_bank_spring_homework1.dto;

import org.springframework.stereotype.Component;
import ua.pervuy.client_bank_spring_homework1.dto.account.AccountDtoOut;
import ua.pervuy.client_bank_spring_homework1.dto.customer.CustomerDtoIn;
import ua.pervuy.client_bank_spring_homework1.dto.customer.CustomerDtoOut;
import ua.pervuy.client_bank_spring_homework1.dto.customer.CustomerSimpleDtoOut;
import ua.pervuy.client_bank_spring_homework1.dto.employer.EmployerDtoIn;
import ua.pervuy.client_bank_spring_homework1.dto.employer.EmployerDtoOut;
import ua.pervuy.client_bank_spring_homework1.dto.employer.EmployerSimpleDtoOut;
import ua.pervuy.client_bank_spring_homework1.entity.Account;
import ua.pervuy.client_bank_spring_homework1.entity.Customer;
import ua.pervuy.client_bank_spring_homework1.entity.Employer;

import java.util.List;
import java.util.Set;

@Component
public class Mapper {

    public CustomerDtoOut map(Customer customer){
        CustomerDtoOut customerDtoOut = new CustomerDtoOut();

        customerDtoOut.setId(customer.getId());
        customerDtoOut.setAge(customer.getAge());
        customerDtoOut.setName(customer.getName());
        customerDtoOut.setEmail(customer.getEmail());

        customerDtoOut.setAccounts(customer.getAccounts()
                .stream()
                .map(this::map)
                .toList());

        customerDtoOut.setEmployers(customer.getEmployers()
                .stream()
                .map(this::mapSimple)
                .toList());

        return customerDtoOut;
    }

    public CustomerSimpleDtoOut mapSimple(Customer customer){
        CustomerSimpleDtoOut customerSimpleDtoOut = new CustomerSimpleDtoOut();

        customerSimpleDtoOut.setId(customer.getId());
        customerSimpleDtoOut.setName(customer.getName());
        customerSimpleDtoOut.setAge(customer.getAge());
        customerSimpleDtoOut.setEmail(customer.getEmail());

        return customerSimpleDtoOut;

    }

    public AccountDtoOut map(Account account){
        AccountDtoOut accountDtoOut = new AccountDtoOut();

        accountDtoOut.setId(account.getId());
        accountDtoOut.setCurrency(account.getCurrency());
        accountDtoOut.setBalance(account.getBalance());
        accountDtoOut.setNumber(account.getNumber());
        accountDtoOut.setCustomerId(account.getCustomer().getId());
        accountDtoOut.setCustomerName(account.getCustomer().getName());

        return accountDtoOut;
    }

    public Customer map(CustomerDtoIn customerDtoIn){
        return new Customer(customerDtoIn.getName(),
                customerDtoIn.getEmail(),
                customerDtoIn.getAge());
    }

    public EmployerDtoOut map(Employer employer){
        EmployerDtoOut employerDtoOut = new EmployerDtoOut();

        employerDtoOut.setId(employer.getId());
        employerDtoOut.setName(employer.getName());
        employerDtoOut.setAddress(employer.getAddress());

        employerDtoOut.setCustomers(employer.getCustomers()
                .stream()
                .map(this::mapSimple)
                .toList());

        return employerDtoOut;
    }

    public EmployerSimpleDtoOut mapSimple(Employer employer){
        EmployerSimpleDtoOut employerSimpleDtoOut = new EmployerSimpleDtoOut();

        employerSimpleDtoOut.setId(employer.getId());
        employerSimpleDtoOut.setAddress(employer.getAddress());
        employerSimpleDtoOut.setName(employer.getName());

        return employerSimpleDtoOut;
    }

    public Employer map(EmployerDtoIn employerDtoIn){
        Employer employer = new Employer();
        employer.setName(employerDtoIn.getName());
        employer.setAddress(employerDtoIn.getAddress());
        return employer;
    }
}
