package ua.pervuy.client_bank_spring_homework1.dto;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;
import ua.pervuy.client_bank_spring_homework1.dto.account.AccountDtoOut;
import ua.pervuy.client_bank_spring_homework1.dto.customer.CustomerDtoIn;
import ua.pervuy.client_bank_spring_homework1.dto.customer.CustomerDtoOut;
import ua.pervuy.client_bank_spring_homework1.dto.customer.CustomerSimpleDtoOut;
import ua.pervuy.client_bank_spring_homework1.dto.employer.EmployerDtoIn;
import ua.pervuy.client_bank_spring_homework1.dto.employer.EmployerDtoOut;
import ua.pervuy.client_bank_spring_homework1.dto.employer.EmployerSimpleDtoOut;
import ua.pervuy.client_bank_spring_homework1.entity.Account;
import ua.pervuy.client_bank_spring_homework1.entity.Customer;
import ua.pervuy.client_bank_spring_homework1.entity.Employer;

import java.util.List;
import java.util.Set;

@Component
public class Facade {
    private final ModelMapper modelMapper;
    public Facade() {
        this.modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.STRICT);

        Converter<List<Account>, List<AccountDtoOut>> convertListAccount = (src) -> src.getSource().stream()
                .map(this::map)
                .toList();
        Converter<Set<Employer>, List<EmployerSimpleDtoOut>> convertListEmployer = (src) -> src.getSource().stream()
                .map(this::mapSimple)
                .toList();

        this.modelMapper
                .createTypeMap(Customer.class, CustomerDtoOut.class)
                .addMappings(mapper -> mapper.using(convertListAccount).map(Customer::getAccounts, CustomerDtoOut::setAccounts))
                .addMappings(mapper -> mapper.using(convertListEmployer).map(Customer::getEmployers, CustomerDtoOut::setEmployers));

        Converter<Set<Customer>, List<CustomerSimpleDtoOut>> convertListCustomer = (src) -> src.getSource().stream()
                .map(this::mapSimple)
                .toList();

        this.modelMapper
                .createTypeMap(Employer.class, EmployerDtoOut.class)
                .addMappings(mapper -> mapper.using(convertListCustomer).map(Employer::getCustomers, EmployerDtoOut::setCustomers));

    }

    //Customer
    public CustomerDtoOut map(Customer customer){

        return this.modelMapper.map(customer, CustomerDtoOut.class);
    }
    public CustomerSimpleDtoOut mapSimple(Customer customer){

        return this.modelMapper.map(customer, CustomerSimpleDtoOut.class);

    }
    public Customer map(CustomerDtoIn customerDtoIn){
        return this.modelMapper.map(customerDtoIn, Customer.class);
    }


    //Account
    public AccountDtoOut map(Account account){
        return this.modelMapper.map(account, AccountDtoOut.class);
    }


    //Employer
    public EmployerDtoOut map(Employer employer){

        return this.modelMapper.map(employer, EmployerDtoOut.class);
    }
    public EmployerSimpleDtoOut mapSimple(Employer employer){

        return this.modelMapper.map(employer, EmployerSimpleDtoOut.class);
    }

    public Employer map(EmployerDtoIn employerDtoIn){

        return this.modelMapper.map(employerDtoIn, Employer.class);
    }
}
