package ua.pervuy.client_bank_spring_homework1.dto.account;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import ua.pervuy.client_bank_spring_homework1.entity.Currency;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AccountDtoOut {

    private Long id;
    private String number;
    private Currency currency;
    private Double balance;
    private String customerName;
    private Long customerId;
}
