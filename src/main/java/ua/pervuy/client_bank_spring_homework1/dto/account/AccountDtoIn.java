package ua.pervuy.client_bank_spring_homework1.dto.account;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.Data;
import ua.pervuy.client_bank_spring_homework1.entity.Currency;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AccountDtoIn {

    private Long id;
    @NotBlank
    private Currency currency;
    private String number;
    @PositiveOrZero
    private Integer summa;

    public void setCurrency(String currency) {
        this.currency = Currency.valueOf(currency);
    }
}
