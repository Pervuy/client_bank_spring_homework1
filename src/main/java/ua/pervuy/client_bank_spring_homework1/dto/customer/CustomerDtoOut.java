package ua.pervuy.client_bank_spring_homework1.dto.customer;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import ua.pervuy.client_bank_spring_homework1.dto.employer.EmployerSimpleDtoOut;
import ua.pervuy.client_bank_spring_homework1.dto.account.AccountDtoOut;

import java.util.List;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CustomerDtoOut {

    private Long id;
    private String name;
    private String email;
    private Integer age;
    private String phone;
    //@JsonManagedReference
    private List<AccountDtoOut> accounts;
    private List<EmployerSimpleDtoOut> employers;

}
