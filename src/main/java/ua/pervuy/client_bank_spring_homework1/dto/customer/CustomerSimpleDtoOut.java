package ua.pervuy.client_bank_spring_homework1.dto.customer;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CustomerSimpleDtoOut {

    private Long id;
    private String name;
    private String email;
    private Integer age;
    private String phone;

}
