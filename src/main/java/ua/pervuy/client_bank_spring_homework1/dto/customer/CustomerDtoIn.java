package ua.pervuy.client_bank_spring_homework1.dto.customer;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CustomerDtoIn {

    private Long id;
    @NotBlank
    @Size(min = 2, message = "Name should be more that 2 character")
    private String name;
    @Email(message = "Email is not correct")
    private String email;
    @Min(value = 18, message = "Age should be more that 17")
    private Integer age;

    @Pattern(regexp = "^\\+380\\d{9}$", message = "Phone should be in format '+380999999999'")
    private String phone;

    public CustomerDtoIn() {

    }
}
