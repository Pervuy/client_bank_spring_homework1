package ua.pervuy.client_bank_spring_homework1.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ua.pervuy.client_bank_spring_homework1.exception.AccountNotFoundException;
import ua.pervuy.client_bank_spring_homework1.exception.CustomerNotFoundException;
import ua.pervuy.client_bank_spring_homework1.exception.EmployerNotFoundException;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ApplicationExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, Object> handelInvalidArgument(MethodArgumentNotValidException ex){

        Map<String, Object> errorMap = new HashMap<>();

        ex.getBindingResult().getFieldErrors().forEach(

                error ->{
                    errorMap.put(error.getField(),error.getDefaultMessage());
                }
        );
        return errorMap;
    }

    @ExceptionHandler(CustomerNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, Object> handelBusinessEx(CustomerNotFoundException ex){

        Map<String, Object> errorMap = new HashMap<>();

        errorMap.put("errorMessage",ex.getMessage());
        return errorMap;
    }

    @ExceptionHandler(AccountNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, Object> handelBusinessEx(AccountNotFoundException ex){

        Map<String, Object> errorMap = new HashMap<>();

        errorMap.put("errorMessage",ex.getMessage());
        return errorMap;
    }
    @ExceptionHandler(EmployerNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, Object> handelBusinessEx(EmployerNotFoundException ex){

        Map<String, Object> errorMap = new HashMap<>();

        errorMap.put("errorMessage",ex.getMessage());
        return errorMap;
    }

}
