package ua.pervuy.client_bank_spring_homework1.exception;

public class EmployerNotFoundException extends RuntimeException{

    public EmployerNotFoundException(String message) {
        super(message);
    }
}
