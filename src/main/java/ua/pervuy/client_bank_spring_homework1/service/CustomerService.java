package ua.pervuy.client_bank_spring_homework1.service;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ua.pervuy.client_bank_spring_homework1.dto.*;
import ua.pervuy.client_bank_spring_homework1.dto.account.AccountDtoIn;
import ua.pervuy.client_bank_spring_homework1.dto.account.AccountDtoOut;
import ua.pervuy.client_bank_spring_homework1.dto.customer.CustomerDtoIn;
import ua.pervuy.client_bank_spring_homework1.dto.customer.CustomerDtoOut;
import ua.pervuy.client_bank_spring_homework1.dto.customer.CustomerSimpleDtoOut;
import ua.pervuy.client_bank_spring_homework1.entity.Account;
import ua.pervuy.client_bank_spring_homework1.entity.Customer;
import ua.pervuy.client_bank_spring_homework1.exception.CustomerNotFoundException;
import ua.pervuy.client_bank_spring_homework1.repositiry.AccountRepo;
import ua.pervuy.client_bank_spring_homework1.repositiry.CustomerRepo;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepo customerDao;
    private final AccountRepo accountDao;

    //private final Mapper mapper;
    private final Facade mapper;

    public List<CustomerDtoOut> getAllCustomers(Integer page, Integer size) {
        return customerDao.findAll(PageRequest.of(page, size))
                .stream()
                .map(mapper::map)
                .toList();
    }
    public CustomerDtoOut getUserByID(Long idCustomer) {
      try {
          Customer customer = customerDao.getReferenceById(idCustomer);
          return mapper.map(customer);
      }catch (EntityNotFoundException ex){
          throw new CustomerNotFoundException("Customer is not found with id:" + idCustomer);
      }
    }
    public CustomerSimpleDtoOut save(CustomerDtoIn customerDtoIn){

        Customer customer = customerDao.save(mapper.map(customerDtoIn));

        return mapper.mapSimple(customer);
    }

    public boolean deleteCustomer(Long id){
        customerDao.deleteById(id);
        return true;
    }

    public CustomerDtoOut editeCustomer(CustomerDtoIn customerDtoIn) {
        //TODO
        try {
            Customer customer = customerDao.getReferenceById(customerDtoIn.getId());
            customer.setName(customerDtoIn.getName());
            customer.setEmail(customerDtoIn.getEmail());
            customer.setAge(customerDtoIn.getAge());
            return mapper.map(customerDao.save(customer));
        }catch (EntityNotFoundException ex){
            throw new CustomerNotFoundException("Customer is not found with id:" + customerDtoIn.getId());
        }
    }

    public AccountDtoOut addAccount(Long idCustomer, AccountDtoIn accountDtoIn){
       try {
           Customer customer = customerDao.getReferenceById(idCustomer);

           Account account = accountDao.save(new Account(customer, accountDtoIn.getCurrency()));

           return mapper.map(account);
       }catch (EntityNotFoundException ex){
           throw new CustomerNotFoundException("Customer is not found with id:"+idCustomer);
       }
    }

    public boolean deleteAccount(Long idCustomer, AccountDtoIn accountDtoIn) {
       try{
           Customer customer = customerDao.getReferenceById(idCustomer);
           Optional<Account> accountByNumber = accountDao.findByNumber(accountDtoIn.getNumber());
           if (accountByNumber.isPresent()){
               customer.deleteAccount(accountByNumber.get());
               accountDao.delete(accountByNumber.get());
               return true;
           }else{
               throw new CustomerNotFoundException("Account is not found with number:" + accountDtoIn.getNumber());
           }
       }catch (EntityNotFoundException ex){
           throw new CustomerNotFoundException("Customer is not found with id:"+idCustomer);
       }
    }
}
