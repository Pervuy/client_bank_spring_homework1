package ua.pervuy.client_bank_spring_homework1.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ua.pervuy.client_bank_spring_homework1.dto.account.AccountDtoIn;
import ua.pervuy.client_bank_spring_homework1.dto.TransferMoneyDtoIn;
import ua.pervuy.client_bank_spring_homework1.entity.Account;
import ua.pervuy.client_bank_spring_homework1.repositiry.AccountRepo;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AccountService {
    private final AccountRepo accountDao;

    private boolean topUpAccount(Account account, Integer summa){

        account.setBalance(account.getBalance() + (double)summa/100);
        accountDao.save(account);
        return true;
    }

    private boolean withdraw(Account account, Integer summa){

        account.setBalance(account.getBalance() - (double)summa/100);
        accountDao.save(account);
        return true;
    }
    public boolean topUpAccount(AccountDtoIn accountDtoIn){
        Optional<Account> accountByNumber = accountDao.findByNumber(accountDtoIn.getNumber());
        if (accountByNumber.isEmpty()) return false;

        return topUpAccount(accountByNumber.get(), accountDtoIn.getSumma());
    }

    public boolean withdraw(AccountDtoIn accountDtoIn) {
        Optional<Account> accountByNumber = accountDao.findByNumber(accountDtoIn.getNumber());

        if (accountByNumber.isEmpty()) return false;
        Account account = accountByNumber.get();
        if (!account.isEnoughMoney(accountDtoIn.getSumma())) return false;

        return withdraw(account, accountDtoIn.getSumma());
    }

    public boolean transferMoney(TransferMoneyDtoIn transferMoneyDtoIn) {
        Optional<Account> accountFrom = accountDao.findByNumber(transferMoneyDtoIn.getNumberFrom());
        Optional<Account> accountTo = accountDao.findByNumber(transferMoneyDtoIn.getNumberTo());

        if (accountFrom.isEmpty()) return false;
        if (accountTo.isEmpty()) return false;
        Account account = accountFrom.get();
        Integer summa = transferMoneyDtoIn.getSumma();
        if (!account.isEnoughMoney(summa)) return false;

        return withdraw(account,summa)
                && topUpAccount(accountTo.get(),summa);

    }
}
