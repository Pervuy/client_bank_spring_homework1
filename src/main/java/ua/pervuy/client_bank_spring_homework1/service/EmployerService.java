package ua.pervuy.client_bank_spring_homework1.service;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ua.pervuy.client_bank_spring_homework1.dto.Facade;
import ua.pervuy.client_bank_spring_homework1.dto.customer.CustomerDtoIn;
import ua.pervuy.client_bank_spring_homework1.dto.employer.EmployerDtoIn;
import ua.pervuy.client_bank_spring_homework1.dto.employer.EmployerDtoOut;
import ua.pervuy.client_bank_spring_homework1.dto.employer.EmployerSimpleDtoOut;
import ua.pervuy.client_bank_spring_homework1.entity.Customer;
import ua.pervuy.client_bank_spring_homework1.entity.Employer;
import ua.pervuy.client_bank_spring_homework1.exception.CustomerNotFoundException;
import ua.pervuy.client_bank_spring_homework1.exception.EmployerNotFoundException;
import ua.pervuy.client_bank_spring_homework1.repositiry.CustomerRepo;
import ua.pervuy.client_bank_spring_homework1.repositiry.EmployerRepo;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployerService {

    private final EmployerRepo employerDao;
    private final CustomerRepo customerDao;
    private final Facade mapper;
    public List<EmployerDtoOut> getAllEmployers(Integer page, Integer size) {
       return employerDao.findAll(PageRequest.of(page, size))
               .stream()
               .map(mapper::map)
               .toList();
    }
    public EmployerDtoOut getEmployerByID(Long idEmployer) {
        try {
            Employer customer = employerDao.getReferenceById(idEmployer);
            return mapper.map(customer);
        } catch (EntityNotFoundException ex) {
            throw new EmployerNotFoundException("Employer is not found with id:" + idEmployer);
        }
    }

    public EmployerSimpleDtoOut save(EmployerDtoIn employerDtoIn) {

        return mapper.mapSimple(employerDao.save(mapper.map(employerDtoIn)));

    }

    public EmployerDtoOut editeEmployer(EmployerDtoIn employerDtoIn) {
        try {
            Employer employer = employerDao.getReferenceById(employerDtoIn.getId());
            employer.setName(employerDtoIn.getName());
            employer.setAddress(employerDtoIn.getAddress());
            return mapper.map(employerDao.save(employer));
        } catch (EntityNotFoundException ex) {
            throw new EmployerNotFoundException("Employer is not found with id:" + employerDtoIn.getId());
        }
    }

    public boolean deleteEmployer(Long idEmployer) {
        employerDao.deleteById(idEmployer);
        return true;
    }

    public EmployerDtoOut addCustomerFromEmployer(Long idEmployer, CustomerDtoIn customerDtoIn) {

        Customer customer;
        try {
            customer = customerDao.getReferenceById(customerDtoIn.getId());
        } catch (EntityNotFoundException ex) {
            throw new CustomerNotFoundException("Customer is not found with id:" + customerDtoIn.getId());
        }

        try {
            Employer employer = employerDao.getReferenceById(idEmployer);
            employer.getCustomers().add(customer);
            Employer saveEmployer = employerDao.save(employer);

            return mapper.map(saveEmployer);
        } catch (EntityNotFoundException ex) {
            throw new EmployerNotFoundException("Employer is not found with id:" + idEmployer);
        }

    }

    public EmployerDtoOut deleteCustomerFromEmployer(Long idEmployer, CustomerDtoIn customerDtoIn) {

        Customer customer;
        try {
            customer = customerDao.getReferenceById(customerDtoIn.getId());
        } catch (EntityNotFoundException ex) {
            throw new CustomerNotFoundException("Customer is not found with id:" + customerDtoIn.getId());
        }

        try {
            Employer employer = employerDao.getReferenceById(idEmployer);
            employer.getCustomers().remove(customer);
            Employer saveEmployer = employerDao.save(employer);
            return mapper.map(saveEmployer);
        } catch (EntityNotFoundException ex) {
            throw new EmployerNotFoundException("Employer is not found with id:" + idEmployer);
        }
    }
}
