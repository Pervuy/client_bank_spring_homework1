package ua.pervuy.client_bank_spring_homework1.repositiry;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.pervuy.client_bank_spring_homework1.entity.Account;
import ua.pervuy.client_bank_spring_homework1.entity.Employer;

public interface EmployerRepo extends JpaRepository<Employer, Long> {

}
